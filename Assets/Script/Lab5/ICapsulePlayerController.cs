using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICapsulePlayerController
{
    void MoveForward();
    void MoveForwardSprint();

    void MoveBackward();

    void TurnLeft();
    void TurnRight();
}
