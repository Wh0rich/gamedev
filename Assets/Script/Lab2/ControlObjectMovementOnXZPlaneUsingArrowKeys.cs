using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ControlObjectMovementOnXZPlaneUsingArrowKeys : StepMovement
{
    void Update()
    {
        Keyboard keyboard = Keyboard.current;

        //GetKey
        if (keyboard[Key.LeftArrow].isPressed)
        {
            MoveLeft();
        }
        else if (keyboard[Key.RightArrow].isPressed)
        {
            MoveRight();
        }
        else if (keyboard[Key.UpArrow].isPressed)
        {
            MoveForward();
        }
        else if (keyboard[Key.DownArrow].isPressed)
        {
            MoveBackward();
        }
    }
}
