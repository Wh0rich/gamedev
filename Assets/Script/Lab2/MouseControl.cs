using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MouseControl : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private GameObject prefab;
    private Vector3 m_MousePreviousPosition;
    public float m_MouseDeltaVectorScaling = 0.5f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PrimitiveType type = PrimitiveType.Sphere;
        PrimitiveType type2 = PrimitiveType.Quad;
        Vector3 mouseCurrentPos = Input.mousePosition;
        Vector3 mouseDeltaVector = Vector3.zero;
        mouseDeltaVector = (mouseCurrentPos - m_MousePreviousPosition).normalized;
        if (Input.GetMouseButtonDown(0))
        {
            
            Debug.Log(Input.GetMouseButtonDown(0));
            var newGameObject =GameObject.CreatePrimitive(type);
            var rigidbody = newGameObject.AddComponent<Rigidbody>();
            Destroy(newGameObject, 2);
        }
        if (Input.GetMouseButtonUp(0))
        {
            var newGameObject =GameObject.CreatePrimitive(type2);
            var rigidbody = newGameObject.AddComponent<Rigidbody>();
           
            Destroy(newGameObject, 2);
        }
        m_MousePreviousPosition = mouseCurrentPos;
    }
}
