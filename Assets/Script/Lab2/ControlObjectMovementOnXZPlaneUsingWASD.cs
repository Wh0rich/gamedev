using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlObjectMovementOnXZPlaneUsingWASD : StepMovement
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            MoveLeft();
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            MoveRight();
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            MoveForward();
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            MoveBackward();
        }
    }
}
