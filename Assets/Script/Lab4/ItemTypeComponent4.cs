using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTypeComponent4 : MonoBehaviour
{
    [SerializeField]
    protected ItemType4 m_ItemType;
    public ItemType4 Type
    {
        get
        {
            return m_ItemType;
        }
        set
        {
            m_ItemType = value;
            
        }
    }
}
