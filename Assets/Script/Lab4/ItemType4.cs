using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 public enum ItemType4
 {
     COIN,
     BIGCOIN,
     POWERUP,
     POWERDOWN,
     SPEEDUP,
     SPEEDDOWN
}
