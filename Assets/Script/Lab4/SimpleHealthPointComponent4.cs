using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleHealthPointComponent4 : MonoBehaviour
{
    [SerializeField] public const float MAX_HP = 100;

    [SerializeField] private float m_HealthPoint;

    //Property
    public float HealthPoint
    {
        get { return m_HealthPoint; }
        set
        {
            m_HealthPoint = value;
            if (m_HealthPoint >= MAX_HP)
            {
                m_HealthPoint = MAX_HP;
            }

            if (m_HealthPoint <= 0)
            {
                m_HealthPoint = 0;
            }
            // if(value > 0)
            // {
            // if(value <= MAX_HP)
            // {
            // m_HealthPoint = value;
            // }else{
            // m_HealthPoint = MAX_HP;
            // }
            // }
        }
    }
}
