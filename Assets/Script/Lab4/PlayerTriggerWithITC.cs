using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

public class PlayerTriggerWithITC : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //Get components from item object
        //Get the ItemTypeComponent component from the triggered object
        ItemTypeComponent4 itc = other.GetComponent<ItemTypeComponent4>();

        //Get components from the player
        //Inventory
        var inventory = GetComponent<Inventory>();
        //SimpleHealthPointComponent
        var simpleHP = GetComponent<SimpleHealthPointComponent4>();
        var player = GetComponent<CapsulePlayerController>();

        if (itc != null)
        {
            switch (itc.Type)
            {
                case ItemType4.COIN:
                    inventory.AddItem("COIN",1);
                    break;
                case ItemType4.BIGCOIN:
                    inventory.AddItem("BIGCOIN",1);
                    break;
                case ItemType4.POWERUP:
                    if (simpleHP != null)
                        simpleHP.HealthPoint = simpleHP.HealthPoint + 10;
                        Debug.Log("HP: " +simpleHP.HealthPoint);
                    break;
                case ItemType4.POWERDOWN:
                    if (simpleHP != null)
                        simpleHP.HealthPoint = simpleHP.HealthPoint - 10;
                        Debug.Log("HP: " +simpleHP.HealthPoint);
                    break;
                case ItemType4.SPEEDUP:
                    player.MDirectionalSpeed = 10;
                    Debug.Log("I'm   S P E E D !!!");
                    break;
                case ItemType4.SPEEDDOWN:
                    player.MDirectionalSpeed = 0.5f;
                    Debug.Log("I'm   S L O W !!!");
                    break;
            }
        }


        Destroy(other.gameObject, 0);
    }
}
